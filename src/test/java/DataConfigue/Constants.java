package DataConfigue;


import org.springframework.stereotype.Component;

@Component
public class Constants {
    public static final String Rapport_Path = "target/generated_Report";
    public static final String PATTERN_DATE = "dd-MM-yyyy";
    public static final String FULL_PATTERN_DATE = "dd-MM-yyyy HH:mm:ss";
    public static final String MAIL_SMTP_HOST= "mail.smtp.host";
    public static final String MAIL_SMTP_PORT= "mail.smtp.port";
}
