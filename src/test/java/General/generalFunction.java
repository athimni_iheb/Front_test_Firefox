package General;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.util.ResourceUtils;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

public class generalFunction{

    private ThreadLocal<WebDriver> webDriver ;
    public static long PAGE_LOAD_TIMEOUT = 20;
    public static long IMPLICIT_WAIT = 10;
    public static WebDriver driver ;
    public static Properties prop ;



    public generalFunction(){

        }

        public void propertySet(){
            driver = null ;
            try {
                prop = new Properties();
                loadProperties(new File(readFileFromResources("src/test/java/DataConfigue/config.properties")));
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        }


        public void loadProperties(File file) throws IOException {
            prop.load(new FileInputStream(file));
        }

        //find the property file from the path and return exact Path
        public String readFileFromResources(String uri) throws FileNotFoundException {
            File toReturn = ResourceUtils.getFile(uri);
            boolean exist = toReturn.exists();
            if (!exist) {
                try {
                    toReturn = new File(getClass().getClassLoader().getResource(uri).toURI());
                }catch (URISyntaxException exp)
                {
                    exp.printStackTrace();
                }
            }
            return toReturn.getAbsolutePath();
        }

        public static void getTheDriver(){

            WebDriverManager.firefoxdriver().setup();

            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(true);
            driver = new FirefoxDriver(options);


            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
//            System.out.println("Driver initialisation");
        }

        public void openUrl(String ult ){
            driver.get(ult);
        }

        public void navigate(final String url) {
            driver.navigate().to(url);
        }


        public void cleanCash() {
            driver.manage().deleteAllCookies();
        }

        public static void removeDriver(){
            if(driver != null){
                driver.quit();
                driver = null ;
            }
        }



    /**
     * Takes a screenshot from the current page
     *
     * @return the bytes of the screenshot
     */

    public static byte[] captureScreenshot(WebDriver driver) {
        if (driver instanceof TakesScreenshot) {
            TakesScreenshot screenshotDriver = (TakesScreenshot) driver;
            return screenshotDriver.getScreenshotAs(OutputType.BYTES);
        }
        return null;
    }











}
