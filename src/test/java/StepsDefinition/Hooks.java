package StepsDefinition;

import General.generalFunction;

import io.cucumber.java.Before;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;


public class Hooks extends generalFunction{
    @Before
    public void Initialisation(){
        propertySet();
        System.out.println("step 0 :set the internal property ");
    }


    @After
    public void takeScreenShot (Scenario scenario) throws IOException {

        scenario.log("this is my screenShot Action");
        // Specify the destination where you want to save the screenshot, relative to your project directory
        String screenshotsDirectory = Paths.get("screenshots").toAbsolutePath().toString();
        new File(screenshotsDirectory).mkdirs(); // Create the directory if it doesn't exist

        // Construct the screenshot file Name
        String screenshotFileName = scenario.getName().replaceAll(" ", "_") + ".png";
        scenario.log(screenshotFileName);

        // Construct the screenshot file path
        String screenshotDestination = Paths.get(screenshotsDirectory, screenshotFileName).toString();
        scenario.log(screenshotDestination);


        // Capture screenshot
        TakesScreenshot ts = (TakesScreenshot) driver;
        File screenshotFile = (ts.getScreenshotAs(OutputType.FILE));

        // Copy the screenshot to the destination
        FileUtils.copyFile(screenshotFile, new File(screenshotDestination));

        // Attach the screenshot to the Cucumber report
        byte[] screenshotBytes = FileUtils.readFileToByteArray(new File(screenshotDestination));
        scenario.attach(screenshotBytes, "image/png", scenario.getName());

        if (driver != null) {
            removeDriver();
//            System.out.println("Closing the browser ");
        }
    }


    @After
    public void TearDown(){
        if(driver != null){
            removeDriver();
            System.out.println("Closing the browser ");
        }

    }

}
